﻿assetApp.service("assetSvc", function ($http) {
    this.getAssetList = function (data, url) {
        var response = $http({
            method: "GET",
            url: url,
            params: { "id": 1, "pageIndex": data.pageIndex, "pageSize": data.pageSize }
           
        });
        return response;
    }

    this.getAsset = function (data, url) {
        var response = $http({
            method: "Get",
            url: url,
            params: { "id": data }

        });
        return response;
    }
});