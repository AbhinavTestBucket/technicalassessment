﻿assetApp.controller('assetAppCtrl', ['$scope', 'assetSvc','uiGridConstants',

    function ($scope, assetSvc, uiGridConstants) {

        $scope.pagemodel = {};
        $scope.pagemodel.pageIndex = 1;
        $scope.pagemodel.pageSize = 25;
        $scope.data = [];
        $scope.NoData = false;

        assetSvc
            .getAssetList($scope.pagemodel, "api/Asset/GetList")
            .then(function (response) {

                console.log(response);
                $scope.data = response.data;
                $scope.gridOptions = {
                    data: $scope.data,
                    paginationPageSizes: [25, 50, 75],
                    paginationPageSize: 25,
                    useExternalPagination: true,
                    showGridFooter: true,
                    enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                    enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                    columnDefs: [
                        { field: 'Id', displayName: "", cellTemplate: 'view.html', width: 80, enableSorting: false, enableFiltering: false },
                        {
                            field: 'FileName',
                            displayName: "Title",
                            enableFiltering: false

                        },
                        {
                            field: 'CreatedBy',
                            displayName: "Created By",
                            enableFiltering: false

                        },

                        { field: 'CreatedOn', displayName: 'CreatedOn' }


                    ],
                    onRegisterApi: function (gridApi) {
                        $scope.gridApi = gridApi;
                        gridApi.pagination.on.paginationChanged($scope,
                            function (newPage, pageSize) {

                                $scope.pagemodel.pageIndex = newPage;
                                $scope.pagemodel.pageSize = pageSize;

                                assetSvc
                                    .getAssetList($scope.pagemodel, "api/Asset/GetList")
                                    .then(function (response) {
                                       
                                        $scope.gridOptions.data = response.data;

                                    });
                            });
                    }
                }

                if ($scope.gridOptions.data.length == 0) {
                    $scope.gridOptions.totalItems = 0;
                    $scope.NoData = true;
                }

                else
                    $scope.NoData = false;
                    $scope.gridOptions.totalItems = $scope.gridOptions.data[0].TotalRecords;

            });


       

        $scope.viewAsset = function (id) {
            assetSvc
                .getAsset(id, "api/Asset/GetAsset" )
                .then(function (response) {

                    $scope.SelectedAsset = response.data;
                    $scope.AssetReadMode = true;

                });



        }

        $scope.toggleBack = function () {

            $scope.AssetReadMode = false;
        }

   
    }]);