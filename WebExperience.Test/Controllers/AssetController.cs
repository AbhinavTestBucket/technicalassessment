﻿using GeneralKnowledge.Test.App.Tests;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebExperience.Test.Controllers
{
    public class AssetController : ApiController
    {

        [HttpGet]
        public IPagedList<Asset> GetList(int id = 1, int pageIndex = 1, int pageSize = 25)
        {

            DbOperations dbOperations = new DbOperations();

            var list = dbOperations.GetPages(pageIndex, pageSize);

            return list;

        }


        [HttpGet]
        public Asset GetAsset(int id)
        {
            DbOperations dbOperations = new DbOperations();

            return dbOperations.GetAsset(id);

        }

    }


    }

