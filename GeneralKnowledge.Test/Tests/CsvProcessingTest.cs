﻿using Dapper;
using PagedList;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralKnowledge.Test.App.Tests
{
    /// <summary>
    /// CSV processing test
    /// </summary>
    /// 


public class DbOperations
 {

private SqlConnection Connection()
   {

            return new SqlConnection(ConfigurationManager.ConnectionStrings["DbConnection"].ConnectionString);


 }


        public IPagedList<Asset> GetPages(int pageIndex = 1, int pageSize = 25)
        {
            using (IDbConnection db = Connection())
            {
                if (db.State == ConnectionState.Closed)
                    db.Open();
                var query = db.QueryMultiple("SELECT COUNT(*) FROM Asset;SELECT Id, FileName, CreatedBy, CreatedOn FROM Asset ORDER BY Id OFFSET ((@PageNumber - 1) * @Rows) ROWS FETCH NEXT @Rows ROWS ONLY", new { PageNumber = pageIndex, Rows = pageSize }, commandType: CommandType.Text);
                var row = query.Read<int>().First();
                var pageResult = query.Read<Asset>().ToList();
                if (pageResult.Count() > 0)
                pageResult[0].TotalRecords = row;
                return new StaticPagedList<Asset>(pageResult, pageIndex, pageSize, row);
            }
        }


        public Asset GetAsset(int id)
        {

            using (IDbConnection db = Connection())
            {
                Asset result = db.Query<Asset>(@"
                    SELECT * 
                    FROM Asset
                    WHERE Id = @Id",
                    new { Id = id }).FirstOrDefault();

                result.CountryName = GetCountry(result.CountryId).Name;
                result.MimeTypeName = GetMimeType(result.MimeTypeId).Name;

                return result;
            }

        }


        public Country GetCountry(long id)
        {

            using (IDbConnection db = Connection())
            {
                Country result = db.Query<Country>(@"
                    SELECT * 
                    FROM Country
                    WHERE Id = @Id",
                    new { Id = id }).FirstOrDefault();
                return result;
            }

        }


        public MimeType GetMimeType(long id)
        {

            using (IDbConnection db = Connection())
            {
                MimeType result = db.Query<MimeType>(@"
                    SELECT * 
                    FROM MimeType
                    WHERE Id = @Id",
                    new { Id = id }).FirstOrDefault();
                return result;
            }

        }

        public void AddAsset(Asset asset)
 {
            using (IDbConnection db = Connection())
            {
                string insertQuery = @"INSERT INTO [dbo].[Asset]([AssetId],[FileName], [CountryId], [MimeTypeId], [CreatedBy], [Email], [Description], [CreatedOn]) VALUES (@AssetId, @FileName, @CountryId, @MimeTypeId, @CreatedBy, @Email, @Description, @CreatedOn)";

                var result = db.Execute(insertQuery, asset);
            }


}

     public int AddCountry(Country country)
        {
            using (IDbConnection db = Connection())
            {
                string insertQuery = @"INSERT INTO [dbo].[Country]([Name]) VALUES (@Name); SELECT CAST(SCOPE_IDENTITY() as int)";
           
                return  db.Query<int>(insertQuery, country).Single();
           }


       }


        public void AddAssets(List<Asset> assetsList)
        {
            Console.WriteLine("Processing CSV..................");

            using (IDbConnection db = Connection())
            {
               db.Execute("INSERT INTO Asset VALUES (@AssetId, @FileName, @CountryId, @MimeTypeId, @CreatedBy, @Email, @Description, @CreatedOn)", assetsList);

                Console.WriteLine("Processing of CSV Completed, Inserted {0} new Records", assetsList.Count());
            }
        }


        public int AddMimeType(MimeType mime)
        {
            using (IDbConnection db = Connection())
            {
                string insertQuery = @"INSERT INTO [dbo].[MimeType]([Name]) VALUES (@Name); SELECT CAST(SCOPE_IDENTITY() as int)";

                return db.Query<int>(insertQuery, mime).Single();
            }


        }


        public IEnumerable<Country> GetCountryList()
        {

            using (IDbConnection db = Connection())
            {
                string selectQuery = @"SELECT * FROM [dbo].[Country]";

                return db.Query<Country>(selectQuery).ToList();
            }

        }

        public IEnumerable<MimeType> GetMimeList()
        {
            var result = new List<MimeType>();

            using (IDbConnection db = Connection())
            {
                string selectQuery = @"SELECT * FROM [dbo].[MimeType]";

                 return db.Query<MimeType>(selectQuery).ToList();
            }

        }




    }




 public class Asset
 {
        public long Id { get; set; }     
        public string AssetId { get; set; }
        public string FileName { get; set; }
        public long   CountryId { get; set; }
        public long   MimeTypeId { get; set; }
        public string CreatedBy { get; set; }
        public string Email { get; set; }
        public string Description { get; set; }
        public string CreatedOn { get; set; }
        public string MimeTypeName { get; set; }
        public string CountryName { get; set; }
        public int TotalRecords { get; set; }

    }

public class MimeType
 {
        public long Id { get; set; }
        public string Name { get; set; }
      
 }

    public class Country
    {
        public long Id { get; set; }
        public string Name { get; set; }

    }


    public class CsvProcessingTest : ITest
    {
        public void Run()
        {
            // TODO: 
            // Create a domain model via POCO classes to store the data available in the CSV file below
            // Objects to be present in the domain model: Asset, Country and Mime type
            // Process the file in the most robust way possible
            // The use of 3rd party plugins is permitted

            var csvFile = Resources.AssetImport;

            DbOperations dbOperations = new DbOperations();

            var countriesList = dbOperations.GetCountryList().ToList();

            var mimeTypeList = dbOperations.GetMimeList().ToList();

            var lines = csvFile.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries).Skip(1);

            var assets = new List<Asset>();

            foreach (var item in lines)
            {
                var mimeId = 0;
                var countryId = 0;
                var values = item.Split(',');
                var mimeExist = mimeTypeList.Where(x => x.Name == values[2]).FirstOrDefault();
                if(mimeExist == null)
                {
                    mimeId = dbOperations.AddMimeType(new MimeType { Name = values[2] });
                    mimeTypeList.Add(new MimeType { Name = values[2], Id = mimeId });
                }

                var countryExist = countriesList.Where(x => x.Name == values[5]).FirstOrDefault();

                if (countryExist == null)
                {
                    countryId = dbOperations.AddCountry(new Country { Name = values[5] });
                    countriesList.Add(new Country { Name = values[2], Id = countryId });
                }

                var model = new Asset
                {
                    AssetId = values[0],
                    FileName = values[1],
                    MimeTypeId = mimeExist != null ? mimeExist.Id : mimeId,
                    CreatedBy = values[3],
                    Email = values[4],
                    CountryId = countryExist != null ? countryExist.Id : countryId,
                    Description = values[6],
                    CreatedOn = DateTime.Now.ToString("dd-MMM-yyyy HH:mm:ss tt")
                };

               

                assets.Add(model);
               
            }

            dbOperations.AddAssets(assets);


        }
    }



}
