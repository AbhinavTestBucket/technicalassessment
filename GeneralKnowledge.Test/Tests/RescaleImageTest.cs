﻿using ImageProcessor;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using ImageProcessor.Imaging.Formats;
using System.IO;
using System.Reflection;

namespace GeneralKnowledge.Test.App.Tests
{
    /// <summary>
    /// Image rescaling
    /// </summary>
    public class RescaleImageTest : ITest
    {
        List<Size> si = new List<Size>()
        {
            new Size(100,80),
            new Size(1200,1600)
        };

        public void Run()
        {
            // TODO:
            // Grab an image from a public URL and write a function thats rescale the image to a desired format
            // The use of 3rd party plugins is permitted
            // For example: 100x80 (thumbnail) and 1200x1600 (preview)

            byte[] imageBytes;
            string someUrl = "https://vastphotos.com/files/uploads/photos/10139/jersey-city-skyline-xl.jpg";
            using (var webClient = new WebClient())
            {
                imageBytes = webClient.DownloadData(someUrl);
            }

            var format = new JpegFormat();

            MemoryStream inStream = new MemoryStream(imageBytes);


            using (ImageFactory imageFactory = new ImageFactory())
            {
                // Load, resize, set the quality and save an image.

                for (int i = 0; i < si.Count(); i++)
                {

                    imageFactory.Load(inStream)
                                .Resize(si[i])
                                .Quality(90)
                                .Save(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "/pic" + si[i].ToString() + ".jpeg"));

                }

            }



        }
    }
}

