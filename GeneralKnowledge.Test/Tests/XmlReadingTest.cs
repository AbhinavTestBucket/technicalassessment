﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace GeneralKnowledge.Test.App.Tests
{
    /// <summary>
    /// This test evaluates the 
    /// </summary>
    public class XmlReadingTest : ITest
    {
        public string Name { get { return "XML Reading Test";  } }



        public void Run()
        {
            var xmlData = Resources.SamplePoints;

            // TODO: 
            // Determine for each parameter stored in the variable below, the average value, lowest and highest number.
            // Example output
            // parameter   LOW AVG MAX
            // temperature   x   y   z
            // pH            x   y   z
            // Chloride      x   y   z
            // Phosphate     x   y   z
            // Nitrate       x   y   z

            PrintOverview(xmlData);
        }

        private void PrintOverview(string xml)
        {        
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(xml);

            var temperatureNodesList = xmlDoc.SelectNodes("/samples/measurement/param[@name='temperature']").Cast<XmlNode>().Select(x => x.InnerText).ToList().Select(x => double.Parse(x)).ToList();
            var pHNodesList = xmlDoc.SelectNodes("/samples/measurement/param[@name='pH']").Cast<XmlNode>().Select(x => x.InnerText).ToList().Select(x => int.Parse(x)).ToList();
            var chlorideNodesList = xmlDoc.SelectNodes("/samples/measurement/param[@name='Chloride']").Cast<XmlNode>().Select(x => x.InnerText).ToList().Select(x => int.Parse(x)).ToList();
            var phosphateNodesList = xmlDoc.SelectNodes("/samples/measurement/param[@name='Phosphate']").Cast<XmlNode>().Select(x => x.InnerText).ToList().Select(x => int.Parse(x)).ToList();
            var nitrateNodesList = xmlDoc.SelectNodes("/samples/measurement/param[@name='Nitrate']").Cast<XmlNode>().Select(x => x.InnerText).ToList().Select(x => int.Parse(x)).ToList();

            
            string header = $"parameter  LOW AVG MAX";


            string temperatureValues = $"temperature  {temperatureNodesList.Min()} {temperatureNodesList.Average()} {temperatureNodesList.Max()} ";
            string phValues = $"pH  {pHNodesList.Min()} {pHNodesList.Average()} {pHNodesList.Max()} ";
            string chlorideValues = $"Chloride  {chlorideNodesList.Min()} {chlorideNodesList.Average()} {chlorideNodesList.Max()} ";
            string phosphateValues = $"Phosphate  {phosphateNodesList.Min()} {phosphateNodesList.Average()} {phosphateNodesList.Max()} ";
            string nitrateValues = $"Nitrate  {nitrateNodesList.Min()} {nitrateNodesList.Average()} {nitrateNodesList.Max()} ";

            Console.WriteLine(header);
            Console.WriteLine(temperatureValues);
            Console.WriteLine(phValues);
            Console.WriteLine(chlorideValues);
            Console.WriteLine(phosphateValues);
            Console.WriteLine(nitrateValues);




        }
    }

   
}
